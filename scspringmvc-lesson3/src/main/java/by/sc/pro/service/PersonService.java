package by.sc.pro.service;

import by.sc.pro.domain.Person;
import java.util.List;

public interface PersonService {
    List<Person> getList();
    void add(Person person);
}
