package by.softclub.pro;

import by.softclub.pro.base.BaseException;
import by.softclub.pro.exceptions.CarNotFoundException;
import by.softclub.pro.exceptions.CurrencyNotFoundException;
import by.softclub.pro.interfaces.Converter;
import org.springframework.context.ApplicationContext;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Optional;

public class CurrencyExchange {

    private HashMap<String, Double> carPrices = new HashMap<>();
    private HashMap<String, String> currCodes = new HashMap<>();

    private double getCarPriceByType(String carType) throws CarNotFoundException{
        Optional<Double> res = Optional.ofNullable(carPrices.get(carType.toUpperCase()));
        return res.orElseThrow(CarNotFoundException::new);
    }

    private String getCurrencyConvByCode(String currCode) throws CurrencyNotFoundException {
        Optional<String> res = Optional.ofNullable(currCodes.get(currCode.toUpperCase()));
        return res.orElseThrow(CurrencyNotFoundException::new);
    }

    public BigDecimal convertPrice(String currCode, String carType, ApplicationContext context) throws BaseException {

        Converter conv = context.getBean(getCurrencyConvByCode(currCode), Converter.class);
        return conv.convert(getCarPriceByType(carType));
    }

    public HashMap<String, Double> getCarPrices() {
        return carPrices;
    }

    public void setCarPrices(HashMap<String, Double> carPrices) {
        this.carPrices = carPrices;
    }

    public HashMap<String, String> getCurrCodes() {
        return currCodes;
    }

    public void setCurrCodes(HashMap<String, String> currCodes) {
        this.currCodes = currCodes;
    }
}
