package by.softclub.pro;

import by.softclub.pro.base.BaseException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.math.BigDecimal;

public class TestSpring {
    /* applicationConfig.properties
    carPrices={\
      'CHEAP': 100,\
      'GOOD': 200,\
      'COOL': 800\
      }

    currencyCodes={\
      'USD': 'usdConverter',\
      'EUR': 'eurConverter',\
      'RUB': 'rubConverter'\
  }
    * */
    public static void main(String[] args) throws BaseException, FileNotFoundException {
        ApplicationContext context = new ClassPathXmlApplicationContext(
            "applicationContext.xml"
    );

        CurrencyExchange converter = context.getBean("currencyExchange", CurrencyExchange.class);
        BigDecimal res = converter.convertPrice(args[0], args[1], context);
        if (args.length < 3) {
            System.out.println(res);
        } else {
            try (PrintWriter out = new PrintWriter(args[2])) {
                out.println(res);
            }
        }
    }
}
