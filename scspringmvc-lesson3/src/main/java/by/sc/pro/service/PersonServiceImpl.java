package by.sc.pro.service;

import by.sc.pro.domain.Person;

import java.util.*;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import org.springframework.stereotype.Service;

@Service
public class PersonServiceImpl implements PersonService {

    private final Set<Person> list = new TreeSet<>(Comparator.comparingInt(Person::getId));

    @PostConstruct
    public void init() {
        list.add(new Person(1, "John", 32));
        list.add(new Person(3, "Max", 25));
        list.add(new Person(2, "Alex", 22));
        list.add(new Person(4, "Jack", 37));
    }

    @Override
    public List<Person> getList() {
        return this.list.stream()
                .sorted(Comparator.comparingInt(Person::getAge).reversed())
                .collect(Collectors.toList());
    }

    @Override
    public void add(Person person) {
            list.add(person);
    }
}
