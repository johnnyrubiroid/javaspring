package by.softclub.pro;

import by.softclub.pro.base.BaseConverter;
import java.math.BigDecimal;

public class RubConverter extends BaseConverter {
    private BigDecimal exchangeRate;

    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public BigDecimal convert(double sumToConvert) {
        return super.convert(sumToConvert, exchangeRate);
    }
}
