package by.softclub.pro.base;

import by.softclub.pro.interfaces.Converter;

import java.math.BigDecimal;

public abstract class BaseConverter  implements Converter {

    public BigDecimal convert(double sumToConvert, BigDecimal exchangeRate) {
        return exchangeRate.multiply(BigDecimal.valueOf(sumToConvert));
    }
}
