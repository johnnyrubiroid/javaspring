package by.softclub.pro.interfaces;

import java.math.BigDecimal;

public interface Converter {

    BigDecimal convert(double sumToConvert, BigDecimal exchangeRate);

    BigDecimal convert(double sumToConvert);
}
